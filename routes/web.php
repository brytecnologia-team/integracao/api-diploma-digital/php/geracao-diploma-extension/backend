<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/XMLDiplomado/inicializa', 'DiplomaController@inicializaXMLDiplomado');
Route::post('/XMLDiplomado/finaliza', 'DiplomaController@finalizaXMLDiplomado');
Route::post('/XMLDiplomado/copia-nodo', 'DiplomaController@copiarNodo');

Route::post('/XMLDocumentacaoAcademica/inicializa', 'DiplomaController@inicializaXMLDocumentacao');
Route::post('/XMLDocumentacaoAcademica/finaliza', 'DiplomaController@finalizaXMLDocumentacao');