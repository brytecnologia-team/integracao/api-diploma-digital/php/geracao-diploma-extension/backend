<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiplomaController extends Controller
{
    // Token Authorization de autenticação gerado no BRy Cloud.
    const token = "TOKEN_BRY_CLOUD";
    // URLs de inicialização e finalização de assinaturas.
    const urlInicializacao = "https://diploma.hom.bry.com.br/api/xml-signature-service/v2/signatures/initialize";
    const urlFinalizacao = "https://diploma.hom.bry.com.br/api/xml-signature-service/v2/signatures/finalize";
    #const urlInicializacao = "https://diploma.bry.com.br/api/xml-signature-service/v2/signatures/initialize";
    #const urlFinalizacao = "https://diploma.bry.com.br/api/xml-signature-service/v2/signatures/finalize";

    // Função chamada na inicialização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function inicializaXMLDiplomado(Request $request)
    {
        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        // Cria array de dados que serão enviados na requisição
        $dados = array(
            'nonce' => '1',
            'signatureFormat ' => 'ENVELOPED',
            'hashAlgorithm' => 'SHA256',
            'certificate' => $request->certificate,
            'originalDocuments[0][nonce]' => '1'
        );

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['originalDocuments[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-diploma-digital.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['originalDocuments[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Adiciona dados referentes ao tipo de assinante
        switch ($request->tipoAssinante) {
            // Assinatura do tipo Representantes
            case 'Representantes':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosRegistroNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosRegistro';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                break;
            // Assinatura do tipo IES Registradora
            case 'IESRegistradora':
                $dados['profile'] = 'ADRA';
                $dados['includeXPathEnveloped'] = 'false';
                break;
            default:
                return '{"message" : "Erro ao identificar o tipo de assinante em assinatura do Diploma."}';
            }

            //Cria a requisição CURL que será enviada
            curl_setopt_array($curl, array(
                CURLOPT_URL => self::urlInicializacao,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $dados,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . self::token
                ),
              ));

        // Envia a requisição para a API de Assinatura BRy
        $respostaInicializacao = curl_exec($curl);
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($respostaInicializacao);
        // Encerra a requisição
        curl_close($curl);

        // Retorna resposta da API para o front.
        // A resposta da inicialização será usada no frontend para cifrar os dados usando a BRy Extension.
        // Em caso de erro, a mensagem de erro sera mostrada no frontend.
        return response()->json($respostaJson);
    }

    // Função chamada na finalização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function finalizaXMLDiplomado(Request $request)
    {
        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        // Cria array de dados que serão enviados na requisição
        $dados = array(
            'nonce' => '1',
            'binaryContent' => 'false',
            'operationType' => 'SIGNATURE',
            'profile' => $request->tipoAssinante == 'IESRegistradora' ? 'ADRA' : 'ADRC',
            'includeXPathEnveloped' => $request->tipoAssinante == 'IESRegistradora' ? 'false' : 'true',
            'signatureFormat ' => 'ENVELOPED',
            'hashAlgorithm' => 'SHA256',
            'certificate' => $request->certificate,
            'finalizations[0][nonce]' => '1',
            'finalizations[0][signatureValue]' => $request->cifrado,
            'finalizations[0][initializedDocument]' => $request->initializedDocuments
        );

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['finalizations[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-diploma-digital.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['finalizations[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Cria a requisição que será enviada para a finalização da assinatura.
        curl_setopt_array($curl, array(
        CURLOPT_URL => self::urlFinalizacao,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $dados,
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . self::token
        ),
        ));

        // Envia a requisição para a API de Assinatura BRy.
        $respostaFinalizacao = curl_exec($curl);
        // Recebe o código HTTP da requisição.
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON.
        $respostaJson = json_decode($respostaFinalizacao);
        // Encerra a requisição.
        curl_close($curl);

        // Se foi enviado um documento junto com a requisição retorna a resposta da API para o frontend
        //      para que seja realizado Download do arquivo.
        // Senão retorno código HTTP 200 para o frontend e armazena o arquivo na pasta "storage"
        // Em caso de erro retorna a resposta da API para o front
        if($httpcode == 200) {
            if ($tipoRetorno == 'BASE64') {
                $arquivoAssinado = fopen(storage_path() . '/XMLsAssinados/exemplo-diplomado-assinado.xml', 'wb');
                fwrite($arquivoAssinado,  base64_decode($respostaJson[0]));
                return '{"message" : "Arquivo salvo localmente."}';
            } else {
                return response()->json($respostaJson);
            }
        } else {
            return response()->json($respostaJson);
        }
    }

    public function inicializaXMLDocumentacao(Request $request)
    {
        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        // Cria array de dados que serão enviados na requisição
        $dados = array(
        'nonce' => '1',
        'signatureFormat ' => 'ENVELOPED',
        'hashAlgorithm' => 'SHA256',
        'certificate' => $request->certificate,
        'originalDocuments[0][nonce]' => '1');

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['originalDocuments[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-documentacao.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['originalDocuments[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Adiciona dados referentes ao tipo de assinante
        switch ($request->tipoAssinante) {
            // Assinatura do tipo IES Emissora Representantes
            case 'Representantes':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosDiploma';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                break;

            // Assinatura do tipo IES Emissora no Nodo Dados Diploma
            case 'IESEmissoraDadosDiploma':
                $dados['profile'] = 'ADRC';
                #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
                $dados['originalDocuments[0][specificNode][name]'] = 'DadosDiploma';
                $dados['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd';
                $dados['includeXPathEnveloped'] = 'false';
                break;

            // Assinatura do tipo IES Emissora para registro
            case 'IESEmissoraRegistro':
                $dados['profile'] = 'ADRA';
                $dados['includeXPathEnveloped'] = 'false';
                break;

            default:
                return '{"message" : "Erro ao identificar o tipo de assinante em assinatura de Documentação Academica."}';
            }

            //Cria a requisição CURL que será enviada
            curl_setopt_array($curl, array(
                CURLOPT_URL => self::urlInicializacao,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $dados,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: " . self::token
                ),
              ));

        // Envia a requisição para a API de Assinatura BRy
        $respostaInicializacao = curl_exec($curl);
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($respostaInicializacao);
        // Encerra a requisição
        curl_close($curl);

        // Retorna resposta da API para o front.
        // A resposta da inicialização será usada no frontend para cifrar os dados usando a BRy Extension.
        // Em caso de erro, a mensagem de erro sera mostrada no frontend.
        return response()->json($respostaJson);
    }

    // Função chamada na finalização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function finalizaXMLDocumentacao(Request $request)
    {
        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        // Cria array de dados que serão enviados na requisição
        $dados = array(
            'nonce' => '1',
            'binaryContent' => 'false',
            'operationType' => 'SIGNATURE',
            'profile' => $request->tipoAssinante == 'IESEmissoraRegistro' ? 'ADRA' : 'ADRC',
            'includeXPathEnveloped' => $request->tipoAssinante == 'Representantes' ? 'true' : 'false',
            'signatureFormat ' => 'ENVELOPED',
            'hashAlgorithm' => 'SHA256',
            'certificate' => $request->certificate,
            'finalizations[0][nonce]' => '1',
            'finalizations[0][signatureValue]' => $request->cifrado,
            'finalizations[0][initializedDocument]' => $request->initializedDocuments);

        //Se recebeu um arquivo na request, adiciona o arquivo. Senão adiciona o XML armazenado
        $tipoRetorno = "";
        if (is_null($request->documento) || $request->documento == "null" || $request->documento == "") {
            $tipoRetorno = "BASE64";

            $dados['finalizations[0][content]'] = new \CURLFILE(storage_path() . '/XMLsOriginais/exemplo-xml-documentacao.xml');
            $dados['returnType'] = $tipoRetorno;
        } else {
            $tipoRetorno = "LINK";

            $dados['finalizations[0][content]'] = new \CURLFILE($request->documento);
            $dados['returnType'] = $tipoRetorno;
        }

        // Cria a requisição que será enviada para a finalização da assinatura.
        curl_setopt_array($curl, array(
        CURLOPT_URL => self::urlFinalizacao,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $dados,
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . self::token
        ),
        ));

        // Envia a requisição para a API de Assinatura BRy.
        $respostaFinalizacao = curl_exec($curl);
        // Recebe o código HTTP da requisição.
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON.
        $respostaJson = json_decode($respostaFinalizacao);
        // Encerra a requisição.
        curl_close($curl);

        // Se a requisição retornou o código HTTP 200, ou seja, sucesso
        //      , ele retorna para o front a requisição, se foi enviado um arquivo ou uma mensagem, se foi utilizado o arquivo local.
        // Caso não tenha retornado 200, retorna a resposta vindo da API.
        if ($httpcode == 200) {
            if ($tipoRetorno == 'BASE64') {
                $arquivoAssinado = fopen(storage_path() . '/XMLsAssinados/exemplo-documentacao-assinado.xml', 'wb');
                fwrite($arquivoAssinado,  base64_decode($respostaJson[0]));
                return '{"message" : "Arquivo salvo localmente."}';
            } else {
                return response()->json($respostaJson);
            }

        } else {
            return response()->json($respostaJson);
        }
    }

    public function copiarNodo(Request $request)
    {
        return '{"message":"Operação não suportada pelo back-end. Realize a copia do nodo manuelmente ou utilize os exemplos em Python ou Java."}';
    }
}
