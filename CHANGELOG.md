# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-07-20

### Added

- Example of diploma generation. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-diploma-extension/backend/-/tags/1.0.0

## [1.1.0] - 2020-08-04

### Added

- Support for private XML signature. 

[1.1.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-diploma-extension/backend/-/tags/1.1.0

## [1.2.0] - 2020-10-29

### Added

- Fix finalization error

[1.2.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-diploma-extension/backend/-/tags/1.2.0

## [1.3.0] - 2022-04-27

### Added

- Fix Diploma to version 1.04

- Accept files to sign from Frontend

[1.3.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-diploma-extension/backend/-/tags/1.3.0



